import java.util.LinkedList;
import java.util.List;

public class Comentarios
{
    private String texto;
    public Usuario usuario;
    int like;
    List<Usuario> listaLikeComentario;


    public Comentarios(String texto,Usuario Vusuario)
    {
        this.texto = texto;
        this.usuario=Vusuario;
        this.like=0;
        listaLikeComentario=new LinkedList<>();
    }

    public void mostrarComentario()
    {
        System.out.println("->Comentario:"+texto);
        System.out.println("Usuario:"+usuario.GetUsuario());
        System.out.println("Like:"+like);
        for (int i=0;i<listaLikeComentario.size();i++)
        {
            System.out.println(" "+listaLikeComentario.get(i).GetUsuario()+" ");
        }
        System.out.println("\n");
    }

    public void Like(Usuario u)
    {
        this.like++;
        this.listaLikeComentario.add(u);
    }

    boolean Letra(char ch){
        if ((ch>='a' && ch<='z')||(ch>='A' && ch<='Z'))
            return true;
        return false;
    }

    public int numeroLetras()
    {
        char ch;
        int cont=0;
        for (int i=0;i<texto.length();i++)
        {
            ch=texto.charAt(i);
            if (Letra(ch))
                cont++;
        }
        return  cont;
    }
}
