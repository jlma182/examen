import java.util.LinkedList;
import java.util.List;

public class Blog extends SitioWeb
{
    String nombreBlog;
    String url;
    private List<Articulo> articulos;
    private Usuario usuario;


    public Blog(String nombreBlog, String url)
    {
        this.url = url;
        this.nombreBlog = nombreBlog;
    }

    public void mostrarBlog()
    {
        System.out.printf(" blog: "+url);
        System.out.printf("      ");
        System.out.printf(" nombre blog: "+nombreBlog+"\n");
    }

    public void anadirArticulo(Articulo articulo)
    {
        articulos.add(articulo);
    }

    //public void borrarArticulo(Articulo articulo)
    //{
     //   articulos.remove(articulo);
    //}
}